# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

from random import randint


# To generate random 8 digit Student Number
def random_with_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


# To Create an Student
def createStudent():
    students["StudentNumber"] = random_with_N_digits(8)
    students["Name"] = str(input("Enter the Student name :"))
    students["Course"] = input("Enter the Course for the student :")
    student_list.append(students)
    print("Student Created Successfully")


# To get Lowest Order Student
def retrieveStudent():
    print("The student record with the lowest student number is :" + str(
        sorted(student_list, key=lambda k: k['StudentNumber'])[0]))


# To show all DBS Student List
def showAll():
    print("DBS Students List :\n " + str(
        sorted(student_list, key=lambda k: k['StudentNumber'])).replace("[", "").replace("},", "},\n").replace("]",
                                                                                                               ""))


# Creating a list to store the students dictionary
student_list = []
# Creating a static variable for Looping
i = 0

print("\nWelcome to DBS College")
try:
    while i <= 3:
        print("\n\n0.Add an Student\n1.Retrieve the lowest "
              "Student Number\n2.Show all the Students\n3.Cancel")
        decision = int(input("\nPlease choose your decision :"))
        if decision == 0:
            students = {}
            createStudent()
        elif decision == 1:
            retrieveStudent()
        elif decision == 2:
            showAll()
        else:
            print("Application Closed, Thank you for using it")
            break
except ValueError:
    print("Invalid Entry")
