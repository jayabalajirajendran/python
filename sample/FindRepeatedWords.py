# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop


from collections import Counter


def retrieveMostFrequentlyUsedWords(literatureText, wordsToExclude):
    for i in range(0, len(wordsToExclude)):
        if wordsToExclude[i] in literatureText:
            literatureText = literatureText.replace(wordsToExclude[i] + ' ', '')
    statement = literatureText.split(' ')
    repeated_word = []
    dict = Counter(statement)
    for key in statement:
        if dict[key] > 1:
            repeated_word.append(key)
    repeated_word = list(set(repeated_word))
    print(repeated_word)


retrieveMostFrequentlyUsedWords("romeo art is flower and was a with romeo", ["is ,was"])
