# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

def find_item():
    value = input("Enter the item name ")
    try:
        print("The value of " + value + " is " + file_dictionary[value.lower()])
        pass
    except KeyError:
        key_name = ""
        for name, value in file_dictionary.items():
            key_name += name + ","
        print("Sorry, Your item not found in the dictionary. It contains " + key_name.rstrip(",") + " only.")


file_dictionary = {}

yes_value = {"yes", "yeah", "yup"}

file_name = input("Enter the file name ")

try:
    with open("/Users/jb/PycharmProjects/resource/" + file_name + ".txt") as f:
        for line in f:
            (key, val) = line.split()
            file_dictionary[str(key)] = val
        print("File found successfully")
        find_item()
        var = input("Do you want to continue? ")
        if var in yes_value:
            find_item()
        else:
            print("Thank you")

except IOError:
    print("File not found")
