# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

import json


def loadAssets():
    with open('/Users/jb/LocalDisk/DBS/PycharmProjects/resource/portfolio.json') as f:
        json_data = json.load(f)
        for key in json_data:
            assets[key['name']] = Portfolio(key['key'], key['values'], key['quality'], key['quantity'], key['name'])


def writeAssets(name, value, type):
    with open('/Users/jb/LocalDisk/DBS/PycharmProjects/resource/portfolio.json', "r") as f:
        json_file = json.load(f)
    if type.upper() == "INVEST":
        for key in json_file:
            if key['name'].upper() == name.upper():
                key['quantity'] = round(key['quantity' + value], 2)
                with open('/Users/jb/LocalDisk/DBS/PycharmProjects/resource/portfolio.json', "w") as f:
                    json.dump(json_file, f)
                    f.truncate()
                return key['quality']
    else:
        for key in json_file:
            if key['name'].upper() == name.upper():
                key['quantity'] = round(key['quantity'] - value, 2)
                with open('/Users/jb/LocalDisk/DBS/PycharmProjects/resource/portfolio.json', "w") as f:
                    json.dump(json_file, f)
                    f.truncate()
                return key['quality']


def quality(name):
    class_object = assets.get(name)
    print("the quality of " + name + " is ", Portfolio.getQuality(class_object))


def price(name):
    class_object = assets.get(name)
    print("the quality of " + name + " is ", Portfolio.getValue(class_object))


def bubbleSort(portfolios):
    for i in range(len(portfolios) - 1):
        for j in range(len(portfolios) - i - 1):
            if portfolios[j].getValue() > portfolios[j + 1].getValue():
                portfolios[j], portfolios[j + 1] = portfolios[j + 1], portfolios[j]
    return portfolios


class Portfolio:
    def __init__(self, n, values, quality, quantity, name):
        self.__key = n
        self.__values = values
        self.__quality = quality
        self.__quantity = quantity
        self.__name = name

    def getName(self):
        return self.__name

    def getValue(self):
        return round(self.__values * Portfolio.getQuantity(self), 2)

    def getQuality(self):
        return self.__quality

    def getQuantity(self):
        return self.__quality


assets = {}
portfolioList = []
get_name = []


def portfolioFunction(i):
    loadAssets()
    # get Portfolio List
    if i == 0:
        for k, v in assets.items():
            get_name.append(v.getName())
        print("Portfolio List are ", get_name)
    # get Particular Value
    elif i == 1:
        for k, v in assets.items():
            get_name.append(v.getName())
        print("Portfolio List are ", get_name)
        name = input("Enter your portfolio name for value :")
        price(name.upper())
    # get Particular Quality
    elif i == 2:
        for k, v in assets.items():
            get_name.append(v.getName())
        print("Portfolio List are ", get_name)
        name = input("Enter your portfolio name for quality :")
        quality(name.upper())
    # Sort by Values
    elif i == 3:
        for k, v in assets.items():
            get_name.append("{0}- {1}".format(v.getName(), v.getValue()))
        print("Portfolio Lists are ", get_name)
        print("Sorting Based on Value")
        for k, v in assets.items():
            portfolioList.append(v)
        bubbleSort(portfolioList)
        for i in portfolioList:
            print(i.getName(), " -", i.getValue())
    elif i == 4:
        for k, v in assets.items():
            get_name.append("{0}- {1}".format(v.getName(), v.getValue()))
        print("Portfolio Lists are ", get_name)
        portfolio_name = input("Enter the Portfolio name to invest :")
        amount = int(input("Enter the amount to invest on :" + portfolio_name.upper()))
        print("Amount invested successfully for {0} and quantity is {1}".format(portfolio_name,
                                                                                writeAssets(portfolio_name, amount,
                                                                                            "INVEST")))
    elif i == 5:
        for k, v in assets.items():
            get_name.append("{0}- {1}".format(v.getName(), v.getValue()))
        print("Portfolio Lists are ", get_name)
        portfolio_name = input("Enter the Portfolio name to invest :")
        amount = int(input("Enter the amount to divest on :" + portfolio_name.upper()))
        print("Amount invested successfully for {0} and quantity is {1}".format(portfolio_name,
                                                                                writeAssets(portfolio_name, amount,
                                                                                            "DIVEST")))
    # Exception Handling
    else:
        print("Invalid Entry")


print("\nWelcome to PortFolio Account\n\n0.List of Portfolio\n1.Get PortFolio Value"
      "\n2.Get PortFolio Quality\n3.Sort Portfolio by Value\n4.Invest in Portfolio\n5.Divest in Portfolio\n")
decision = int(input("Enter your decision"))
portfolioFunction(decision)
