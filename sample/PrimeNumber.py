# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

# What is the 10001st prime number?

from math import sqrt


# Find the nth Prime Number value.

def primeNumber(n):
    count = 1
    i = 1
    while count < n:
        i += 2
        for k in range(2, 1 + int(sqrt(i + 1))):
            if i % k == 0:
                break
        else:
            count += 1
    return i


# Find the list of prime number below the range.

def listOfPrimeNumber(n):
    for num in range(1, n + 1):
        if num > 1:
            for i in range(2, num):
                if (num % i) == 0:
                    break
            else:
                print(num)


# Find the sum of all the primes below the range.

def sumOfPrimeNumber(n):
    marked = [0] * n
    value = 3
    s = 2
    while value < n:
        if marked[value] == 0:
            s += value
            i = value
            while i < n:
                marked[i] = 1
                i += value
        value += 2
    print(s)


print(primeNumber(2))
sumOfPrimeNumber(10)
