# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

# Find the sum of all the multiples of 3 or 5 below 1000.

sum = 0
for i in range(1, 1000):
    if i % 3 == 0 or i % 5 == 0:
        sum += i

print("Sum of these multiples are ", sum)
