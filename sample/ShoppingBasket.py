# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

# Standard item price list
shopping_items = {"Egg": 3.00, "Rice": 2.00, "Flour": 2.50}

# quantity of shopping list
quantity_per_items = {"Egg": 8, "Rice": 3, "Flour": 2}

# Discount Coupon code
discount = {"Egg": "eggs20", "Flour": "flour10"}

# Discount Coupon code
discountValue = {"eggs20": 20, "flour10": 10}


# Discount the current price using coupons
def list_of_basket():
    list_of_basket = {}
    for k, v in shopping_items.items():
        if k in discount:
            # getting the discounted price
            discountPrice = (discountValue[discount[k]] / 100) * shopping_items[k] * quantity_per_items[k]
            # getting the final price after discount
            finalValue = shopping_items[k] * quantity_per_items[k] - discountPrice
            # adding it in a new list dictionary
            list_of_basket[k] = finalValue
        else:
            # adding it if no coupons available for the item
            list_of_basket[k] = quantity_per_items[k] * v
    return list_of_basket


# Sort the list from highest to lowest
def sortDictionary(dict):
    for key in sorted(dict.keys(), key=lambda x: x[1]):
        print(key, "-", dict[key], "Euro")


# Main Method
print("\nOur Standard price list -", shopping_items)
print("The shopping list you have taken per item -", quantity_per_items)
print("Discount Coupons you have -", discount)
print("Discount Percentage Value for the valid coupons -", discountValue)
print("\nAfter Discount, The Price list range from highest to lowest is given below")
sortDictionary(list_of_basket())
