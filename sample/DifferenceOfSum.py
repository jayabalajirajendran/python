# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.


def difference_between_sum_and_square(n):
    sum = 0
    square = 0
    for i in range(1, n + 1):
        sum += i * i
    for i in range(1, n + 1):
        square += i
    square *= square
    difference = square - sum
    return difference


print(difference_between_sum_and_square(100))
