# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop


# To Cancel the Application
def cancel():
    print("Transaction Cancelled, Thank you")
    exit(0)


# To Create an Account
def createAccount():
    name = input("Enter the account name :")
    accounts[name.title()] = Account(name.title())
    print("Account Created Successfully")


# To do Deposit
def doDeposit(class_object):
    amount = input("Enter amount to be Deposited: ")
    if class_object.deposit(amount):
        print("\nAmount Deposited to " + class_object.getName() + " :" + class_object.getBalance())


# To do Withdraw
def doWithdraw(class_object):
    amount = input("\nEnter amount to be Withdrawn: ")
    if class_object.withdraw(amount):
        print("Amount Withdraw : " + amount)
        print("Current Available Balance in " + class_object.getName() + " : " + class_object.getBalance())
    else:
        print("Insufficient balance")
        print("Current Available Balance for " + class_object.getName() + " is : " + class_object.getBalance())


# To transfer from one account to another account
def doTransfer(from_account, to_account, from_name, to_name):
    amount = input("\nEnter the amount you wish to transfer " + to_name + " : ")
    if from_account.withdraw(amount):
        print("Amount transferred to " + to_name + " : " + amount)
        print("Available Balance in " + from_name + " is : " + from_account.getBalance())
        to_account.deposit(amount)
    else:
        print("Insufficient balance")
        print("Current Available Balance for " + from_account.getName() + " is :" + from_account.getBalance())


# No Accounts found error
def noAccountsError():
    print("Sorry, No accounts found in that name.")


# Get Account from dictionary
def getAccount(name):
    class_object = accounts.get(name.title())
    return class_object


# Creating a Class name ACCOUNT
class Account:

    def __init__(self, n):
        self.__name = n
        self.__balance = 0

    def getName(self):
        return self.__name

    def getBalance(self):
        return self.__balance

    def deposit(self, amount):
        if amount < 0:
            raise ValueError("Negative Deposits not permitted")
        self.__balance += amount
        return True

    def withdraw(self, amount):
        if amount < 0:
            raise ValueError("Negative Withdrawals not permitted")
        if amount > self.__balance:
            return False
        else:
            self.__balance -= amount
            return True


# Creating a Dictionary to store the accounts
accounts = {}


# Switch case statement based on input given by the user

def bankingFunction(i):
    # Creating Account
    if i == 0:
        createAccount()
    # Accounts you have
    elif i == 1:
        print(
            "Your have these accounts :" + str(accounts.keys()).replace("dict_keys([", "").replace("])", ""))
    # Check Balance
    elif i == 2:
        name = input("Your account name is :")
        if name.title() in accounts:
            print("Your Balance is :" + getAccount(name).getBalance())
        else:
            if bool(accounts):
                print("You dont have any accounts left, Please create an account to proceed")
            else:
                print("Account name you entered is invalid, You have " + str(accounts.keys()).replace("dict_keys([",
                                                                                                      "").replace("])",
                                                                                                                  ""))
    # Do Deposit
    elif i == 3:
        name = input("\nYour account name you wish to deposit :")
        if name.title() in accounts:
            doDeposit(getAccount(name))
        else:
            noAccountsError()
    # Do Withdraw
    elif i == 4:
        name = input("\nYour account name you wish to withdraw from :")
        if name.title() in accounts:
            doWithdraw(getAccount(name))
        else:
            noAccountsError()
    # Do Transfer
    elif i == 5:
        from_name = input("\nEnter the account name to initiate transfer from: ")
        to_name = input("\nEnter the account name to transfer : ")
        if from_name.title() and to_name.title() in accounts.keys():
            from_account = getAccount(from_name.title())
            to_account = getAccount(to_name.title())
            doTransfer(from_account, to_account, from_name, to_name)
        else:
            print("Either from or to account is invalid, Please try again")
    # Cancel Application
    elif i == 6:
        cancel()
    # Exception Handling
    else:
        print("Invalid Entry")


print("\nWelcome to AIB Bank Personal Account\n\n0.Create an Account\n1.Accounts you "
      "have\n2.Balance\n3.Deposit\n4.Withdraw\n5.Transfer\n6.Cancel")
try:
    # Loop Function
    def continueFunction():
        user_value = int(input("\nPlease choose your decision :"))
        bankingFunction(user_value)
        continueFunction()


    decision = int(input("Please choose your decision :"))
    bankingFunction(decision)
    continueFunction()
except NameError:
    print("Invalid Entry")
