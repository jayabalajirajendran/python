# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

# Find the largest palindrome made from the product of two 3-digit numbers.
n = 0
for a in range(999, 100, -1):
    for b in range(a, 100, -1):
        x = a * b
        if x > n:
            s = str(a * b)
            if s == s[::-1]:
                n = a * b
print(n)
