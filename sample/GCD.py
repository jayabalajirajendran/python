import math


def gcd(x, y):
    return y and gcd(y, x % y) or x


print(gcd(5, 10))
