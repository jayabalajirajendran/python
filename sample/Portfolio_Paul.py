# Used PYTHON V3in PYCHARM IDE
# Author Paul
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop


prices = {'EUR': 1.00, 'USD': 1.25, 'GBP': 0.89, 'AAPL': 0.95}


class Portfolio:
    def __init__(self, balance={}):
        self.__balance = balance.copy()

    def invest(self, sym, qty):
        if qty < 0:
            raise ValueError('Negative qty')
        if qty < self.getBalance(sym):
            raise ValueError('Insufficient Fund')
        self.__balance[sym] = qty + self.getBalance(sym)
        print(self.getBalance(sym))

    def divest(self, sym, qty):
        if qty < 0:
            raise ValueError('Negative qty')
        if qty > self.getBalance(sym):
            raise ValueError('Insufficient Fund')
        self.__balance[sym] = self.getBalance(sym) - qty
        print(self.__balance[sym])

    def getBalance(self, sym):
        if sym not in self.__balance:
            return 0
        return self.__balance[sym]

    def getValue(self):
        sum = 0
        for k, v in self.__balance.items():
            sum += v * prices[k]
        return sum

    def bubbleSort(portfolios):
        for i in range(len(portfolios) - 1):
            for j in range(len(portfolios) - i - 1):
                if portfolios[j].getValue() > portfolios[j + 1].getValue():
                    portfolios[j], portfolios[j + 1] = portfolios[j + 1], portfolios[j]
        return portfolios

    def mergeSort(portfolios):
        if len(portfolios) == 1:
            return portfolios
        first = Portfolio.mergeSort(portfolios[:len(portfolios) // 2])
        last = Portfolio.mergeSort(portfolios[len(portfolios) // 2:])
        counter1 = 0
        counter2 = 0
        newList = []
        for i in range(len(portfolios)):
            if counter2 == len(last) or (
                    counter1 != len(first) and first[counter1].getValue() < last[counter2].getValue()):
                newList.append(first[counter1])
                counter1 += 1
            else:
                newList.append(last[counter2])
                counter2 += 1
        return newList

    def quickSort(portfolios):
        if len(portfolios) < 2:
            return
        pivotal = portfolios[len(portfolios) - 1].getValue()
        p1 = 0
        p2 = len(portfolios) - 1
        while p1 < p2:
            while portfolios[p1].getValue() < pivotal and p1 <= p2:
                p1 += 1
            while portfolios[p2].getValue() < pivotal and p2 <= p1:
                p2 -= 1
            if p2 > p1:
                portfolios[p2], portfolios[p1] = portfolios[p1], portfolios[p2]
                p2 -= 1
                p1 += 1
        return Portfolio.quickSort(portfolios[1:p1]) + [portfolios[0] + portfolios[p1]]


p = Portfolio({'EUR': 100})
p.getBalance('EUR')
p.invest('EUR', 200)
p.invest('USD', 100)
p.getValue()
print("\n")
p1 = Portfolio({'EUR': 101})
p2 = Portfolio({'USD': 101})
p3 = Portfolio({'GBP': 104})
p4 = Portfolio({'AAPL': 105})
ps = [p1, p2, p3, p4]
print(ps)
for i in ps:
    print(i.getValue())
print("\n")
Portfolio.bubbleSort(ps)
for i in ps:
    print(i.getValue())
