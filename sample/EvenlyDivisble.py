# Used PYTHON V3in PYCHARM IDE
# Author Jayabalaji Rajendran(10537723)
# BitBucket Link https://bitbucket.org/jayabalajirajendran/python/branch/Develop

# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20

import math

sum = 1
for i in range(1, 21):
    sum = int(sum * i / math.gcd(sum, i))

print("The smallest positive number that is evenly divisible by all of the numbers is", sum)
